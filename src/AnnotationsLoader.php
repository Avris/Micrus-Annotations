<?php
namespace Avris\Micrus\Annotations;

use Avris\Bag\Bag;
use Avris\Micrus\Annotations\Loader\AnnotationsLoaderInterface;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Avris\Micrus\Tool\Config\ArrayHelper;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

final class AnnotationsLoader
{
    /** @var AnnotationReader */
    private $reader;

    /** @var AnnotationsLoaderInterface[] */
    private $loaders;

    /** @var ModuleInterface[] */
    private $modules;

    /** @var CacherInterface */
    private $cacher;

    /** @var ArrayHelper */
    private $arrayHelper;

    public function __construct(
        AnnotationReader $reader,
        array $annotationsLoaders,
        array $modules,
        CacherInterface $cacher,
        ArrayHelper $arrayHelper
    )
    {
        $this->reader = $reader;
        $this->loaders = $annotationsLoaders;
        $this->modules = $modules;
        $this->cacher = $cacher;
        $this->arrayHelper = $arrayHelper;
    }

    public function load(string $loaderClass): Bag
    {
        return $this->cacher->cache($loaderClass, function () use ($loaderClass) {
            $loader = $this->loaders[$loaderClass] ?? null;

            if (!$loader) {
                throw new \RuntimeException(sprintf('AnnotationLoader %s not defined', $loaderClass));
            }

            $dir = $loader->getDir();

            foreach ($this->modules as $module) {
                $this->loadDir(
                    $loader,
                    $module->getName() . '\\' . str_replace('/', '\\', $dir),
                    $module->getDir() . '/src/' . str_replace('\\', '/', $dir)
                );
            }

            return new Bag($loader->get());
        });
    }

    public function extend(Bag $config, string $loaderClass): Bag
    {
        return new Bag(
            $this->arrayHelper->merge(
                $config->all(),
                $this->load($loaderClass)->all()
            )
        );
    }

    private function loadDir(AnnotationsLoaderInterface $loader, string $namespace, string $dir)
    {
        if (!is_dir($dir)) {
            return;
        }

        $finder = (new Finder())
            ->files()
            ->in($dir)
            ->name('*.php')
            ->ignoreDotFiles(true)
            ->ignoreUnreadableDirs(true)
            ->ignoreVCS(true);

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $this->loadClass(
                $loader,
                $this->buildClassName($namespace, $file->getRelativePathname())
            );
        }
    }

    private function buildClassName(string $namespace, string $relativePathName)
    {
        return $namespace . '\\' . strtr(substr($relativePathName, 0, -4), ['/' => '\\']);
    }

    private function loadClass(AnnotationsLoaderInterface $loader, string $className)
    {
        $class = new \ReflectionClass($className);
        foreach ($this->reader->getClassAnnotations($class) as $annotation) {
            $loader->loadClass($class, $annotation);
        }

        foreach ($class->getMethods() as $method) {
            foreach ($this->reader->getMethodAnnotations($method) as $annotation) {
                $loader->loadMethod($method, $annotation);
            }
        }

        foreach ($class->getProperties() as $property) {
            foreach ($this->reader->getPropertyAnnotations($property) as $annotation) {
                $loader->loadProperty($property, $annotation);
            }
        }
    }
}
