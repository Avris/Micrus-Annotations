<?php
namespace Avris\Micrus\Annotations;

use Avris\Micrus\Annotations\Loader\RouteAnnotationsLoader;
use Avris\Micrus\Annotations\Loader\SecureAnnotationsLoader;
use Avris\Micrus\Bootstrap\ConfigExtension;

final class AnnotationsExtension implements ConfigExtension
{
    /** @var AnnotationsLoader */
    private $loader;

    public function __construct(AnnotationsLoader $loader)
    {
        $this->loader = $loader;
    }

    public function extendConfig(): array
    {
        return array_merge(
            $this->loader->load(RouteAnnotationsLoader::class)->all(),
            $this->loader->load(SecureAnnotationsLoader::class)->all()
        );
    }
}
