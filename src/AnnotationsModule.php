<?php
namespace Avris\Micrus\Annotations;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class AnnotationsModule implements ModuleInterface
{
    use ModuleTrait;
}
