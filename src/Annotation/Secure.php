<?php
namespace Avris\Micrus\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 */
final class Secure
{
    /** @var array */
    private $options = [];

    /** @var bool */
    private $public = false;

    public function __construct($values)
    {
        if (isset($values['public']) && $values['public']) {
            $this->public = true;
        }

        foreach (['pattern', 'roles', 'check', 'object'] as $field) {
            if (isset($values[$field])) {
                $this->options[$field] = $values[$field];
            }
        }
    }

    public function getData($target): array
    {
        return array_merge([
            'target' => $target,
        ], $this->options);
    }

    public function isPublic(): bool
    {
        return $this->public;
    }
}
