<?php
namespace Avris\Micrus\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 */
final class Route
{
    /** @var string */
    private $pattern;

    /** @var string */
    private $name;

    /** @var array */
    private $options = [];

    public function __construct($values)
    {
        $this->pattern = $values['value'];

        if (isset($values['name'])) {
            $this->name = $values['name'];
        }

        if (isset($values['options'])) {
            $this->options = $values['options'];
        }
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
