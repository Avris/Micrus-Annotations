<?php
namespace Avris\Micrus\Annotations\Loader;

interface AnnotationsLoaderInterface
{
    public function getDir(): string;

    public function loadClass(\ReflectionClass $class, $annotation);

    public function loadMethod(\ReflectionMethod $method, $annotation);

    public function loadProperty(\ReflectionProperty $property, $annotation);

    public function get(): array;
}
