<?php
namespace Avris\Micrus\Annotations\Loader;

use Avris\Micrus\Annotations\Annotation\Secure;

final class SecureAnnotationsLoader implements AnnotationsLoaderInterface
{
    /** @var array */
    private $publicPaths = [];

    /** @var array */
    private $restrictions = [];

    public function getDir(): string
    {
        return 'Controller';
    }

    public function loadClass(\ReflectionClass $class, $annotation)
    {
        if (!$annotation instanceof Secure) {
            return;
        }

        $this->addSecure($annotation, $class->getName());
    }

    public function loadMethod(\ReflectionMethod $method, $annotation)
    {
        $actionName = $this->testEnding($method->getName(), 'Action');
        if (!$annotation instanceof Secure || !$method->isPublic() || !$actionName) {
            return;
        }

        $className = $method->getDeclaringClass()->getName();
        $target =  $className . '/' . $actionName;

        $this->addSecure($annotation, $target);
    }

    public function loadProperty(\ReflectionProperty $property, $annotation)
    {
    }

    private function addSecure(Secure $annotation, string $target)
    {
        $data = $annotation->getData($target);
        $key = md5(json_encode($data));

        if ($annotation->isPublic()) {
            $this->publicPaths[$key] = $data;
        } else {
            $this->restrictions[$key] = $data;
        }
    }

    private function testEnding($string, $ending)
    {
        return substr($string, strlen($string) - strlen($ending)) == $ending
            ? substr($string, 0, strlen($string) - strlen($ending))
            : false;
    }

    public function get(): array
    {
        return [
            'security' => [
                'restrictions' => $this->restrictions,
                'public' => $this->publicPaths,
            ]
        ];
    }
}
