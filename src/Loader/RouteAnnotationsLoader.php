<?php
namespace Avris\Micrus\Annotations\Loader;

use Avris\Micrus\Annotations\Annotation\Route;
use Avris\Micrus\Controller\Routing\Service\RouteParser;

final class RouteAnnotationsLoader implements AnnotationsLoaderInterface
{
    /** @var RouteParser */
    private $routeParser;

    /** @var array */
    private $routes = [];

    /** @var string[] */
    private $bases = [];

    public function __construct(RouteParser $routeParser)
    {
        $this->routeParser = $routeParser;
    }

    public function getDir(): string
    {
        return 'Controller';
    }

    public function loadClass(\ReflectionClass $class, $annotation)
    {
        if (!$annotation instanceof Route) {
            return;
        }

        $this->bases[$class->getName()] = $annotation->getPattern();
    }

    public function loadMethod(\ReflectionMethod $method, $annotation)
    {
        $actionName = $this->testEnding($method->getName(), 'Action');
        if (!$annotation instanceof Route || !$method->isPublic() || !$actionName) {
            return;
        }

        $className = $method->getDeclaringClass()->getName();
        $target =  $className . '/' . $actionName;

        $this->addRoute($annotation, $target, $this->bases[$className] ?? '');
    }

    public function loadProperty(\ReflectionProperty $property, $annotation)
    {
    }

    private function addRoute(Route $annotation, string $target, string $routeBase)
    {
        $routeName = $annotation->getName() ?: $this->buildRouteName($target);
        $options = $annotation->getOptions();
        $optionsJson = $options ? ' ' . json_encode($options) : '';

        $this->routes[$routeName] = $this->routeParser->parse(
            $routeName,
            $annotation->getPattern() . ' -> ' . $target . $optionsJson,
            empty($options['absolute']) ? $routeBase : ''
        );
    }

    private function buildRouteName($controller)
    {
        // TODO more generic?

        if ($controller === 'App\\Controller\\HomeController/home') {
            return 'home';
        }

        if (preg_match('#App\\\\Controller\\\\(.*)Controller/(.*)#', $controller, $matches)) {
            $controller = lcfirst($matches[1]) . ucfirst($matches[2]);
        }

        if (preg_match('#(.*)Module\\\\Controller\\\\(.*)Controller/(.*)#', $controller, $matches)) {
            $controller = lcfirst($matches[1]) . ucfirst($matches[2]) . ucfirst($matches[3]);
        }

        return lcfirst(preg_replace_callback(
            '/[\\\\\/\-][a-zA-Z]/',
            function($matches) { return strtoupper($matches[0][1]); },
            $controller
        ));
    }

    private function testEnding($string, $ending)
    {
        return substr($string, strlen($string) - strlen($ending)) == $ending
            ? substr($string, 0, strlen($string) - strlen($ending))
            : false;
    }

    public function get(): array
    {
        return [
            'routing' => $this->routes,
        ];
    }
}
