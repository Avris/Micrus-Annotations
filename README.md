# Micrus Annotations

This is a module for [Micrus framework](https://micrus.avris.it)
converts annotations into the framework config.

## Installation

Run:

    composer require avris/micrus-annotations

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\Annotations\AnnotationsModule;


## @M\Route ###

Route definitions are put in the `config/routing.yml` file, like this:

    postList: /post/list -> Post/list
    postRead: /post/{int:id}/read -> Post/read
    postAdd:  /post/add -> Post/form
    postEdit: /post/{int:id}/edit -> Post/form

When this module is enabled, you could put that information directly in the controller:

    <?php
    namespace App\Controller;

    use Avris\Micrus\Controller\Controller;
    use Avris\Micrus\Annotations as M;

    /**
     * @M\Route("/post")
     */
    class PostController extends Controller
    {
        /**
         * @M\Route("/list")
         */
        public function listAction()
        {
            // ...
        }

        /**
         * @M\Route("/{int:id}/read")
         */
        public function readAction(Post $post)
        {
            // ...
        }

        /**
         * @M\Route("/add", name="postAdd")
         * @M\Route("/{int:id}/edit", name="postEdit")
         * @M\Secure(check="canEditPost")
         */
        public function formAction(Post $post = null)
        {
            // ...
        }
    }

Note that:
 * a `use` statement is required,
 * when all your routes in some controller share the same prefix, you can declare it as a class annotation,
 * when no `name` is declared, it will be set to `controller name` + `action name`, eg. `postList`,
 * multiple routes can be attached to one controller.

## @M\Secure

Analogously, authorisation config can also be moved from `config/security.yml` to the annotations:

    public:
      - { pattern: ^/post/restricted/public$ }
    restrictions:
      - { pattern: ^/post/restricted }
      - { pattern: ^/post/admin, roles: [ROLE_ADMIN] }
      - { pattern: ^/post/add$ }
      - { pattern: ^/post/(\d+)/edit$, check: canEditPost }

Will become:

    <?php
    namespace App\Controller;

    use Avris\Micrus\Controller\Controller;
    use Avris\Micrus\Annotations as M;

    class PostController extends Controller
    {
        /**
         * @M\Secure
         */
        public function restrictedAction()
        {
            // ...
        }

        /**
         * @M\Secure(public=true)
         */
        public function restrictedPublicAction()
        {
            // ...
        }

        /**
         * @M\Secure(roles={"ROLE_ADMIN"})
         */
        public function adminAction()
        {
            // ...
        }

        /**
         * @M\Secure(check="canEditPost")
         */
        public function formAction(Post $post = null)
        {
            // ...
        }
    }
    
## Extending

Implement `Avris\Micrus\Annotations\Loader\AnnotationsLoaderInterface` (tag `annotationsLoader`).

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
